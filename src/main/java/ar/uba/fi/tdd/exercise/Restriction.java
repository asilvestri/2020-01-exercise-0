package ar.uba.fi.tdd.exercise;

public class Restriction {

    public int minQuality;
    public int maxQuality;
    public int uniqueQuality;
    public int normalQualityIncrement;
    public int normalSellInIncrement;
    public int multiplier;
    SpecialRestriction[] belowSpecialRestrictions;
    SpecialRestriction[] aboveSpecialRestrictions;

    // constructor
    public Restriction(){
        this.aboveSpecialRestrictions = new SpecialRestriction[]{};
        this.belowSpecialRestrictions = new SpecialRestriction[]{};
    }

    public Restriction(
        int _minQuality, 
        int _maxQuality, 
        int _uniqueQuality, 
        int _normalQualityIncrement,
        int _normalSellInIncrement,
        int _multiplier,
        SpecialRestriction[] _belowSpecialRestrictions,
        SpecialRestriction[] _aboveSpecialRestrictions) {

        this.minQuality = _minQuality;
        this.maxQuality = _maxQuality;
        this.uniqueQuality = _uniqueQuality;
        this.multiplier = _multiplier;
        this.normalQualityIncrement = _normalQualityIncrement;
        this.normalSellInIncrement = _normalSellInIncrement;
        this.belowSpecialRestrictions = _belowSpecialRestrictions;
        this.aboveSpecialRestrictions = _aboveSpecialRestrictions;
    }

    // shows the Item representation
   @Override
   public String toString() {

     return "";
    }
 }
