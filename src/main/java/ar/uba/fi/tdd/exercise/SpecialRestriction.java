package ar.uba.fi.tdd.exercise;

public class SpecialRestriction {

    public int restrictionValue;
    public int incrementValue;

    // constructor
    public SpecialRestriction(
        int _restrictionValue, 
        int _incrementValue) {

        this.restrictionValue = _restrictionValue;
        this.incrementValue = _incrementValue;
    }

    // shows the Item representation
   @Override
   public String toString() {

     return "";
    }
 }
