
package ar.uba.fi.tdd.exercise;

class GildedRose {
    GeneralItem[] generalItems;

    public GildedRose(GeneralItem[] _generalItems) {
        generalItems = _generalItems;
    }

    public void updateQuality(){
        for (int i = 0; i < this.generalItems.length; i++) {
            this.generalItems[i].updateSellIn();
            this.generalItems[i].updateQuality();
            this.generalItems[i].ensureMinQuality();
            this.generalItems[i].ensureMaxQuality();
            this.generalItems[i].ensureUniqueQuality();
        }
    }
}
