package ar.uba.fi.tdd.exercise;

public final class Parameter {
    private Parameter () { 
    }

    public static String itemName_AgedBrie = "Aged Brie";
    public static String itemName_Tafkal80 = "Backstage passes to a TAFKAL80ETC concert";
    public static String itemName_Sulfuras = "Sulfuras, Hand of Ragnaros";
    public static int itemQuality_Min = 0;
    public static int itemQuality_Max = 50;
    public static int itemQuality_NormalIncrement = 1;
    public static int itemQuality_NormalDecrement = 1;
    public static int itemSellIn_NormalIncrement = 1;
    public static int itemSellIn_NormalDecrement = 1;
    public static int uniqueQuality_InvalidValue = 0;
    public static int maxValue = 999;

}
