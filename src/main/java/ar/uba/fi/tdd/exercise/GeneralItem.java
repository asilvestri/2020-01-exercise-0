package ar.uba.fi.tdd.exercise;

public class GeneralItem {

    public Item item;
    public Restriction restriction;

    public GeneralItem(Item _item, Restriction _restriction) {
        this.item = _item;
        this.restriction = _restriction;
    }

    public void updateQuality() {
        this.item.quality += this.restriction.normalQualityIncrement * this.restriction.multiplier;
        updateQualityWithBelowRestriction();
        updateQualityWithAboveRestrictions();
    }

    public void updateQualityWithBelowRestriction() {     
        for (int i = 0; i < this.restriction.belowSpecialRestrictions.length; i++) { 
            if(this.item.sellIn < this.restriction.belowSpecialRestrictions[i].restrictionValue)
                this.item.quality += this.restriction.belowSpecialRestrictions[i].incrementValue * this.restriction.multiplier;
        }
    }
    public void updateQualityWithAboveRestrictions() {        
        for (int i = 0; i < this.restriction.aboveSpecialRestrictions.length; i++) {
            if(this.item.sellIn > this.restriction.aboveSpecialRestrictions[i].restrictionValue)
                this.item.quality += this.restriction.aboveSpecialRestrictions[i].incrementValue * this.restriction.multiplier;
        }
    }

    public void ensureMaxQuality() {
        if(this.item.quality > restriction.maxQuality)
            this.item.quality = restriction.maxQuality;
    }

    public void ensureMinQuality() {
        if(this.item.quality < restriction.minQuality)
            this.item.quality = restriction.minQuality;
    }

    public void ensureUniqueQuality() {
        if(restriction.uniqueQuality != Parameter.uniqueQuality_InvalidValue)
            this.item.quality = restriction.uniqueQuality;
    }

    public void updateSellIn() {
        this.item.sellIn += this.restriction.normalSellInIncrement;
    }

   @Override
   public String toString() {

     return this.item.toString() + ", " + this.restriction.toString();
    }
 }
