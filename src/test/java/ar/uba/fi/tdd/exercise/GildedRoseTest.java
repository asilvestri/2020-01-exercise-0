package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

int minQuality = 0;
int maxQuality = 50;
int sulfurasQuality = 80;

	@Test
	public void Conjured() {
		Restriction restriction = new Restriction(); 
		restriction.minQuality = 0; 
		restriction.maxQuality = 50; 
		restriction.uniqueQuality = 0; 
		restriction.multiplier = 2; 
		restriction.normalQualityIncrement = -1;
		restriction.normalSellInIncrement = -1;
		SpecialRestriction[] belowRestriction = new SpecialRestriction[] {
			new SpecialRestriction(0, -1),
		};
		restriction.belowSpecialRestrictions = belowRestriction;

			GeneralItem[] generalItems = new GeneralItem[] { 
				new GeneralItem(new Item("Conjured", -1, 50), restriction),
				new GeneralItem(new Item("Conjured", -1, 2), restriction),      
			};
			GildedRose app = new GildedRose(generalItems);
			app.updateQuality();
			for (int i = 0; i < app.generalItems.length; i++) {
				assertThat(app.generalItems[i].item.quality).isGreaterThanOrEqualTo(minQuality);
				assertThat(app.generalItems[i].item.quality).isLessThanOrEqualTo(maxQuality);
			}
			assertThat(app.generalItems[0].item.quality).isEqualTo(46);
			assertThat(app.generalItems[1].item.quality).isEqualTo(0);
	}
	
	@Test
	public void agedBrie() {
		Restriction restriction = new Restriction(); 
		restriction.minQuality = 0; 
		restriction.maxQuality = 50; 
		restriction.uniqueQuality = 0;  
		restriction.multiplier = 1;
		restriction.normalQualityIncrement = 1;
		restriction.normalSellInIncrement = -1;
		SpecialRestriction[] belowRestriction = new SpecialRestriction[] {
			new SpecialRestriction(0, 1),
		};
		restriction.belowSpecialRestrictions = belowRestriction;

		GeneralItem[] generalItems = new GeneralItem[] { 
			new GeneralItem(new Item(Parameter.itemName_AgedBrie, 10, 50),restriction),
			new GeneralItem(new Item(Parameter.itemName_AgedBrie, 10, 1),restriction),
			new GeneralItem(new Item(Parameter.itemName_AgedBrie, 10, 49),restriction),
			new GeneralItem(new Item(Parameter.itemName_AgedBrie, -1, 48),restriction),
			};
			GildedRose app = new GildedRose(generalItems);
			app.updateQuality();
			for (int i = 0; i < app.generalItems.length; i++) {
				assertThat(app.generalItems[i].item.quality).isGreaterThanOrEqualTo(minQuality);
				assertThat(app.generalItems[i].item.quality).isLessThanOrEqualTo(maxQuality);
			}
			assertThat(app.generalItems[2].item.quality).isEqualTo(maxQuality);
			assertThat(app.generalItems[3].item.quality).isEqualTo(maxQuality);
	}

	@Test
	public void tafkal() {
		Restriction restriction = new Restriction(); 
		restriction.minQuality = 0; 
		restriction.maxQuality = 50; 
		restriction.uniqueQuality = 0;  
		restriction.multiplier = 1;
		restriction.normalQualityIncrement = 1;
		restriction.normalSellInIncrement = -1;
		SpecialRestriction[] belowRestriction = new SpecialRestriction[] {
			new SpecialRestriction(0, -Parameter.maxValue),
			new SpecialRestriction(5, 1),
			new SpecialRestriction(10, 1),
		};
		restriction.belowSpecialRestrictions = belowRestriction;

		GeneralItem[] generalItems = new GeneralItem[] { 
				new GeneralItem(new Item(Parameter.itemName_Tafkal80, 15, 50),restriction),
				new GeneralItem(new Item(Parameter.itemName_Tafkal80, 15, 1),restriction),
				new GeneralItem(new Item(Parameter.itemName_Tafkal80, 10, 48),restriction),
				new GeneralItem(new Item(Parameter.itemName_Tafkal80, 5, 47),restriction),
				new GeneralItem(new Item(Parameter.itemName_Tafkal80, -1, 20),restriction),
			 };
			GildedRose app = new GildedRose(generalItems);
			app.updateQuality();
			for (int i = 0; i < app.generalItems.length; i++) {
				assertThat(app.generalItems[i].item.quality).isGreaterThanOrEqualTo(minQuality);
				assertThat(app.generalItems[i].item.quality).isLessThanOrEqualTo(maxQuality);
			}
			assertThat(app.generalItems[1].item.sellIn).isEqualTo(14);
			assertThat(app.generalItems[2].item.quality).isEqualTo(maxQuality);
			assertThat(app.generalItems[3].item.quality).isEqualTo(maxQuality);
			assertThat(app.generalItems[4].item.quality).isEqualTo(0);
			assertThat(app.generalItems[4].item.sellIn).isEqualTo(-2);
	}

	@Test
	public void sulfuras() {
		Restriction restriction = new Restriction(); 
		restriction.minQuality = 0; 
		restriction.maxQuality = 50; 
		restriction.uniqueQuality = 80;  
		restriction.multiplier = 1;
		restriction.normalQualityIncrement = 0;
		restriction.normalSellInIncrement = 0;
		
		GeneralItem[] generalItems = new GeneralItem[] { 
				new GeneralItem(new Item(Parameter.itemName_Sulfuras, 0, 80),restriction),
				new GeneralItem(new Item(Parameter.itemName_Sulfuras, 10, 80),restriction),
				new GeneralItem(new Item(Parameter.itemName_Sulfuras, 100, 50),restriction),
				new GeneralItem(new Item(Parameter.itemName_Sulfuras, -1, 80),restriction),
			 };
			 GildedRose app = new GildedRose(generalItems);
			 app.updateQuality();
			 for (int i = 0; i < app.generalItems.length; i++) {
				 assertThat(app.generalItems[i].item.quality).isEqualTo(sulfurasQuality);
			 }
			assertThat(10).isEqualTo(app.generalItems[1].item.sellIn);
			assertThat(100).isEqualTo(app.generalItems[2].item.sellIn);
			assertThat(-1).isEqualTo(app.generalItems[3].item.sellIn);
	}

}
