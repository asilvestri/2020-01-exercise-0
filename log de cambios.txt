* La intención de este archivo es llevar un pequeño listado de los hechos
más redundantes de cada día con respecto al trabajo hecho en el proyecto
sin ser un historial de cambios (para eso tenemos git), sino como una
ayuda-memoria y una orientación rápida sobre los aportes realizados.
-------------------------------------------------------------------------
17/09/2021
-------------------------------------------------------------------------
Test de permisos para pull/commit/push.
-------------------------------------------------------------------------
18/09/2021
-------------------------------------------------------------------------
Instalación de Gradle, build y ejecución de tests.
Variable de entorno: C:\Gradle\gradle-6.9.1\bin
-------------------------------------------------------------------------
19/09/2021
-------------------------------------------------------------------------
Se crearon los test unitarios y se quitó hardcodeo básico.
-------------------------------------------------------------------------
20/09/2021
-------------------------------------------------------------------------
Se agregó lógica para cumplir con el nuevo feature.
-------------------------------------------------------------------------